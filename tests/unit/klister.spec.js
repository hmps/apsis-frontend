


describe("Klister", function() {

    var optionsObj = {
        target: 'target'
    };

    it("should register a module", function() {
        expect(window.Klister).toBeTruthy();
    });

    describe("construction", function() {
        var klister, thrower;

        afterEach(function() {
            thrower = null;
            klister = null;
        });

        it("should throw an error if constructed without options", function() {
            thrower = function() {
                klister = new Klister();
            };

            expect(thrower).toThrow();
        });

        it("should throw an error if constructed without options", function() {
            thrower = function() {
                klister = new Klister({});
            };

            expect(thrower).toThrow();
        });
    });

    it("should be a constructor", function() {
        var klister1 = new Klister(optionsObj)
            klister2 = new Klister(optionsObj);

        klister1.name = 'klister1';
        klister2.name = 'klister2';

        expect(klister1).toBeObject();
        expect(klister1.name).toEqual('klister1');

        expect(klister2).toBeObject();
        expect(klister2.name).toEqual('klister2');
    });

    it("should construct itself if called without new", function() {
        var klister1 = Klister(optionsObj)
            klister2 = Klister(optionsObj);

        klister1.name = 'klister1';
        klister2.name = 'klister2';

        expect(klister1).toBeObject();
        expect(klister1.name).toEqual('klister1');

        expect(klister2).toBeObject();
        expect(klister2.name).toEqual('klister2');
    });

    describe("public methods", function() {
        var klister;

        beforeEach(function() {
            klister = new Klister(optionsObj);
        });

        afterEach(function() {
            klister = null;
        });

        it("should have a destroy method", function() {
            expect(klister.destroy).toBeFunction();
        });

        it("should have a activate method", function() {
            expect(klister.activate).toBeFunction();
        });

        it("should have a deactivate method", function() {
            expect(klister.deactivate).toBeFunction();
        });
    });

});