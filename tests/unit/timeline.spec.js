describe("timeline.module", function() {
    var options = { fwd: true, back: true, list: true, scroll: true, element: true };

    it("should create a amd module if available", function() {
        // window.define = function(name, deps, factory) {
        //     window.amdModule = name;
        //     return {
        //         amd: true
        //     }
    });

    describe("Construction", function() {
        var thrower;

        beforeEach(function() {

            thrower = function(keyToExclude) {
                var _options = helpers.cloneObj(options);
                _options[keyToExclude] = false;

                return {
                    run: function run() {
                        return new Timeline(_options);
                    }
                };
            }

        });

        it("should throw an error if constructed without options", function() {
            var thrower = function() {
                return new Timeline();
            };

            expect(thrower).toThrow();
        });

        it("should throw an error if constructed without options.element", function() {
            expect(thrower('element').run).toThrow();
        });

        it("should throw an error if constructed without options.fwd", function() {
            expect(thrower('fwd').run).toThrow();
        });

        it("should throw an error if constructed without options.back", function() {
            expect(thrower('back').run).toThrow();
        });

        it("should throw an error if constructed without options.list", function() {
            expect(thrower('list').run).toThrow();
        });

        it("should throw an error if constructed without options.scroll", function() {
            expect(thrower('lista').run).toThrow();
        });
    });



    it("should export a Timeline module on the window", function() {
        // var timeline = new Timeline(options);
        // spyOn($.fn, 'on').and.returnValue('click');

        // result = $("#Something").on();
        // expect(result).toEqual('click');
    });
});