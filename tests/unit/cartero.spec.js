


describe("Catero", function() {

    /** -------- SETUP --------- */
    var itRan, testFn;
    it("should register a module", function() {
        expect(cartero).toBeTruthy();
    });

    it("should have a bind method", function() {
        expect(cartero.bind).toBeFunction();
    });

    beforeEach(function() {
        itRan = false;
        testFn = function() {
            itRan = true;
        };
    });

    afterEach(function() {
        cartero.clear('eventName');
    });



    /** -------- TEST --------- */
    it("should reject an event without a callback", function() {
        expect(cartero.bind('eventName')).toBeUndefined();
    });


    it("should not trigger an event that does not exist", function() {
        expect(cartero.trigger('eventName')).toBeUndefined();
    });


    it("should be able to bind and trigger an event", function() {
        cartero.bind('eventName', testFn);
        expect(itRan).toBeFalsy();
        cartero.trigger('eventName');
        expect(itRan).toBeTruthy();
    });


    it("should be able to pass along parameters with an event", function() {
        var sum;
        testFn = function(a, b) {
            itRan = true;
            sum = a+b;
        };

        cartero.bind('eventName', testFn);
        expect(itRan).toBeFalsy();

        cartero.trigger('eventName', 7, 2);
        expect(itRan).toBeTruthy();
        expect(sum).toEqual(9);
    });


    it("should be able to unbind an event", function() {
        cartero.bind('eventName', testFn);
        cartero.trigger('eventName');
        expect(itRan).toBeTruthy();

        itRan = false;
        expect(itRan).toBeFalsy();
        cartero.unbind('eventName', testFn);

        cartero.trigger('eventName');
        expect(itRan).toBeFalsy();
    });


    it("should not be able to unbind an event that does not exist", function() {
        expect(cartero.unbind('eventName')).toBeUndefined();
    });


    it("should not unbind an event without a callback", function() {
        cartero.bind('eventName', testFn);
        cartero.trigger('eventName');
        expect(itRan).toBeTruthy();

        cartero.unbind('eventName');
        itRan = false;
        cartero.trigger('eventName');
        expect(itRan).toBeTruthy();
    });


    it("should be able to clear the bound events", function() {
        cartero.bind('eventName', testFn);
        cartero.trigger('eventName');
        expect(itRan).toBeTruthy();

        itRan = false;
        cartero.clear('eventName');

        cartero.trigger('eventName');
        expect(itRan).toBeFalsy();
    });


    it("should not be able to clear an event that does not exist", function() {
        expect(cartero.clear('eventName')).toBeUndefined();
    });


    it("should have method aliases setup", function() {
        expect(cartero.bind).toEqual(cartero.on);
        expect(cartero.trigger).toEqual(cartero.deliver);
        expect(cartero.unbind).toEqual(cartero.off);
    });
});