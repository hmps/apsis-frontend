module.exports = {
    entry: {
        Timeline: './examples/timeline/timeline-example.js',
        lazyImages: './examples/lazyimg/lazyImages-example.js'
    },
    output: {
        path: './examples/[name]/',
        filename: '[name].js'
    },
    resolve: {
        modulesDirectories: ['modules', 'node_modules', 'bower_components']
    },
    externals: {
        // require('jquery') is external and available
        //  on the global var jQuery
        jquery: 'jQuery'
    }
};
