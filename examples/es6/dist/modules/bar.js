define(["exports"], function (exports) {
  "use strict";

  /* jshint esnext: true */
  // bar.js
  var bar = "bar";

  exports["default"] = bar;
});