define(["exports"], function (exports) {
  "use strict";

  /* jshint esnext: true */
  // foo.js
  var foo = "foo";

  exports["default"] = foo;
});