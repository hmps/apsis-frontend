define(["exports", "./modules/foo", "./modules/bar"], function (exports, _modulesFoo, _modulesBar) {
  "use strict";

  var foo = _modulesFoo["default"];
  var bar = _modulesBar["default"];


  console.log("From module foo >>> ", foo);
  console.log("From module bar >>> ", bar);
});