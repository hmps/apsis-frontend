var $ = require('jquery'),
    Timeline = require('timeline');

var timeline = (function kickoffTimeline() {
    'use strict';
    /* global Timeline */

    var $el = $('#js-timeline');

    return new Timeline({
        element: $el,
        data: getMockData()
    });


    // //////////////////////////////////////////// //


    function getMockData() {
        return {
            data: [
                {
                    date: '2014-11-01',
                    events: [
                        { id: 1, type: 'invitation', number: 3 }
                    ]
                },
                {
                    date: '2014-11-02',
                    events: [
                        { id: 3, type: 'reminder', number: 6 },
                        { id: 2, type: 'event', number: 1 }
                    ]
                },
                {
                    date: '2014-11-03',
                    events: [
                        { id: 5, type: 'reminder', number: 6 },
                    ]
                },
                {
                    date: '2014-11-05',
                    events: [
                        { id: 6, type: 'followup', number: 5 },
                        { id: 3, type: 'reminder', number: 6 },
                    ]
                },
                {
                    date: '2014-11-06',
                    events: [
                        { id: 1, type: 'invitation', number: 3 }
                    ]
                },
                {
                    date: '2014-11-07',
                    events: [
                        { id: 3, type: 'reminder', number: 6 },
                        { id: 2, type: 'event', number: 1 }
                    ]
                },
                {
                    date: '2014-11-08',
                    events: [
                        { id: 5, type: 'reminder', number: 6 },
                    ]
                },
                {
                    date: '2014-11-09',
                    events: [
                        { id: 6, type: 'followup', number: 5 },
                        { id: 3, type: 'reminder', number: 6 },
                    ]
                }
            ]
        };
    }
}());

$('#js-addEvent').on('click', function() {
    timeline.addEvent({
        date: '2014-11-04',
        events: [
            { id: 8, type: 'reminder', number: 1 }
        ]
    });
});

$('#js-removeEvent').on('click', function() {
    console.log(timeline);
});
