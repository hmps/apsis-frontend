;(function(angular) {
    'use strict';

    angular.module('FormmoduleApp')
        .controller('TypeController', typeController);

    function typeController($scope) {
        var vm = this;

        vm.types = [
            {
                'name': 'text',
            },
            {
                'name': 'text area',
            },
            {
                'name': 'checkbox'
            }
        ];

        return vm;
    }


}(angular));