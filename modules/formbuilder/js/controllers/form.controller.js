;(function(angular) {
    'use strict';

    angular.module('FormmoduleApp')
        .controller('FormController', formController);

    function formController($scope) {
        var vm = this;
        
        vm.title = "Prislista";

        vm.items = [
            {
                'name': 'Epost',
                'type': 'text'
            },
            {
                'name': 'Vill du även ha vårt nyhetsbrev',
                'type': 'checkbox'
            }
        ];

        return vm;
    }


}(angular));