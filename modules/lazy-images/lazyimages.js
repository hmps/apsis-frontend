// Execute this on load
module.exports = lazyImage();

function lazyImage() {
    'use strict';

    var imgs = document.querySelectorAll('[data-lazyimg]');

    Array.prototype.forEach.call(imgs, function forEach(el) {
        new Bild(el);
    });

    function Bild(el) {
        var bild = this,
            oldSrc;

        el.addEventListener('load', loadImage);
        el.addEventListener('error', error);
        el.onerror = error;

        bild.render = function render() {
            oldSrc = el.src;
            el.removeEventListener('load', loadImage);
            el.src = el.getAttribute('data-lazyimg');
        };

        function loadImage() {
            bild.render();
        }

        function error() {
            el.src = oldSrc;
        }
    }
}
