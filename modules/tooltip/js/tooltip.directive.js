;(function(angular) {
    'use strict';
    /* global angular, $ */
    /* jshint latedef: false */

    /**
     * Apsis.modules.tooltip
     *
     * @description Module to show / hide a tooltip in the browser. An example
     * implementation is shown below.
     *
     * It supports rich text as well as styling with HTML elements like lists.
     *
     * @params
     *     tooltip          * required
     *     tooltip-delay    * optional
     *
     * @example
     * <div apsis-tooltip="'This is a <strong>tooltip</strong>'></div>"
     *
     */

    angular.module('Apsis.modules.tooltip')
        .directive('apsisTooltip', tooltipDirective);

    function tooltipDirective() {

        return {
            restrict: 'AE',
            scope: {
                tooltip: '@apsisTooltip',
                tooltipDelay: '='
            },
            template: '<div ng-transclude></div>',
            replace: true,
            transclude: true,
            link: activateTooltips
        };
    }


    function activateTooltips(scope, $element) {
        var arrowSize = 8,
            marginToElement = 4,

            active = false,
            delay = scope.delay || 300,

            $tooltip = $('<div></div>');


        // Kickoff the module
        init();


        /**
         * Inititate the tooltip, setting up the floating container and event
         * listeners for the target.
         */
        function init() {
            $tooltip
                .addClass('tooltip')
                .html(scope.tooltip);

            $element.on('mouseenter', requestTooltip);
            $element.on('mouseleave', hideTooltip);
        }



        /**
         * Hide the tooltip and set the state to inactive
         */
        function hideTooltip() {
            active = false;
            $tooltip.removeClass('is-visible');

            // Remove the tooltip from the DOM aswell
            removeTooltipWhenHidden();
        }



        /**
         * Wait for the tooltip to be completely invisible and then remove it
         * from the DOM.
         */
        function removeTooltipWhenHidden() {
            if ( 0 <= $tooltip.css('opacity') ) {
                $tooltip.remove();
            } else {
                setTimeout(function callAgain() {
                    removeTooltipWhenHidden();
                }, 100);
            }
        }



        /**
         * Request a tooltip and show it after the set delay.
         *
         * If the user has moved the mouse from the target element before the
         * tooltip has been shown the show method will never be called.
         */
        function requestTooltip() {
            active = true;
            setTimeout(function showAfterDelay() {
                if ( active ) { showTooltip(); }
            }, delay);
        }



        /**
         * Show the tooltip
         */
        function showTooltip() {
            var pos = $element.offset();

            /**
             * The tooltip will be appended to the body and positionend
             * absolutely by the target element.
             */
            $('body').append($tooltip);

            setTimeout(function timeout() {
                $tooltip
                    .css({
                        left: pos.left + ($element.outerWidth() / 2) - ($tooltip.outerWidth() / 2),
                        top: pos.top - $tooltip.outerHeight() - (arrowSize + marginToElement)
                    })
                    .addClass('is-visible');
            }, 1);
        }
    }

}(angular));
