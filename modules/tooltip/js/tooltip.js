/* global jQuery */
;(function tooltip($) {
    'use strict';

    var template = '<div class="tooltip">{{tooltip}}</div>',
        arrowSize = 8,
        marginToElement = 4,
        delay = 300;

    setTimeout(function waitForDOM() {
        $.each($('[data-apsis-tooltip]'), activateTooltips);
        console.log('run');
    }, 100);

    function activateTooltips(i, el) {
        var $el = $(el),
            $tooltip = $(template),
            tooltip = $el.data('apsisTooltip'),
            active = false,
            html = $tooltip.html().replace(/{{tooltip}}/, tooltip);

            console.log(1);

        $tooltip
            .html(html);

        $el.on('mouseenter', requestTooltip);
        $el.on('mouseleave', hideTooltip);



        function hideTooltip() {
            active = false;
            $tooltip.removeClass('is-visible');
            removeTooltipWhenHidden();
        }



        function requestTooltip() {
            active = true;
            setTimeout(function showAfterDelay() {
                if ( active ) { showTooltip(); }
            }, delay);
        }



        function showTooltip() {
            var pos = $el.offset();

            $('body').append($tooltip);

            setTimeout(function timeout() {
                $tooltip
                    .css({
                        left: pos.left + ($el.outerWidth() / 2) - ($tooltip.outerWidth() / 2),
                        top: pos.top - $tooltip.outerHeight() - (arrowSize + marginToElement)
                    })
                    .addClass('is-visible');
            }, 1);
        }



        function removeTooltipWhenHidden() {
            if ( 0 <= $tooltip.css('opacity') ) {
                $tooltip.remove();
            } else {
                setTimeout(function callAgain() {
                    removeTooltipWhenHidden();
                }, 100);
            }
        }
    }

}(jQuery));
