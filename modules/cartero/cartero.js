/**
 * Cartero is a tiny event emitter for JavaScript applications. It's pretty
 * barebones right now, all it does is allow you to send or receive custom
 * events in JS.
 *
 * Registering for an event is really simple:
 * cartero.on('eventName', function() { // execute when triggered });
 *
 * Broadcasting is just as easy:
 * cartero.trigger('eventName', args);
 *
 * Browser compability: IE8+ (and everything else...);
 *
 * @author: HPE
 */

/* globals define */

(function umd(root, name, factory) {
    'use strict';
    if ( 'function' === typeof define && define.amd) {
        // Register as a named AMD module
        define(name, [], factory);
    } else {
        // Register as a function on the global namespace
        root[name] = factory();
    }
}(this, 'cartero', function factory() {
    'use strict';

    var Cartero = function Cartero() {
        var _events = {};

        return {
            bind: bind,
            clear: clear,
            deliver: trigger,
            on: bind,
            off: unbind,
            send: trigger,
            trigger: trigger,
            unbind: unbind
        };

        /**
         * Bind events to a function.
         *
         * @method bind
         * @param  {string}   event - the event name
         * @param  {Function} fn    - the callback function
         */
        function bind(event, fn) {
            if ( 'function' !== typeof fn ) { return; }

            _events[event] = _events[event] || [];
            _events[event].push(fn);
        }


        /**
         * Remove all callbacks attached to a specific event
         *
         * @method clear
         *
         * @param  {string} event - the event that should be cleared;
         */
        function clear(event) {
            if ( false === event in _events ) { return; }
            _events[event] = [];
        }


        /**
         * Remove binding of a function to an event.
         *
         * @method unbind
         *
         * @param  {string}   event - the event name
         * @param  {Function} fn    - the event callback
         */
        function unbind(event, fn) {
            if (
                false === event in _events ||
                _events[event].indexOf(fn)
            ) {
                return;
            }

            _events[event].splice(_events[event].indexOf(fn), 1);
        }


        /**
         * Trigger an event
         *
         * @method trigger
         *
         * @param  {string} event - the event name
         */
        function trigger(event /* , args... */) {
            if ( false === event in _events ) { return; }

            for ( var i = 0; i < _events[event].length; i++) {
                /* jshint validthis: true */
                _events[event][i].apply(this, Array.prototype.slice.call(arguments, 1));
            }
        }

    };

    return new Cartero();
}));

