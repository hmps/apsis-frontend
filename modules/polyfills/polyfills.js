/* globals define */
;(function umd(root, name, factory) {
    'use strict';
    if ('function' === typeof define && define.amd) {
        define(name, [], factory);
    } else {
        // Register as a function on the global namespace
        root[name] = factory();
    }
}(this, 'polyfills', function UMDFactory() {
    'use strict';

    /**
     * The polyfills needs to be available asap, so we make them an IIFE.
     *
     * @author HPE
     */
    (function polyfills() {

        if (
            !Array.prototype.forEach &&
            !window.jQuery
        ) {
            throw {
                error: 'Missing dependency',
                message: 'The browser needs jQuery for polyfills and it is not available on the global scope'
            };
        }


        Array.prototype.forEach = Array.prototype.forEach || jQuery.each;

    }());
}));
