;(function timelineModule() {
    'use strict';
    /* global angular */

    /**
    * Apsis.modules.timeline
    *
    * Description
    */
    angular.module('Apsis.modules.timeline', [
        'ng'
    ]);

}(angular));
