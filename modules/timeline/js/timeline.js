/**
 * Timeline module
 *
 * @author HPE, KIV
 */

/* globals define, $ */

(function umd(root, name, factory) {
    'use strict';
    if ('function' === typeof define && define.amd) {
        define(name, [], factory);
    } else {
        root[name] = factory();
    }
}(this, 'Timeline', function UMDFactory() {
    'use strict';

    var Timeline = TimelineConstructor;

    // Activate all timelines that are available in the DOM
    $(document).ready(activateTimelines);

    return Timeline;


    /**
     * Constructor for the Timeline module
     *
     * @constructor
     * @param {object} options - an object of options for setup
     */
    function TimelineConstructor(options) {

        checkRequiredParams(options);

        var factory = {
            goForward: goForward,
            goBack: goBack,
            addEvent: addEvent,
            // removeEvent: removeEvent,
            destroy: destroy
        },



        _state = {
            atEnd: false,
            atBeginning: true
        },
        _events = {},
        _elements = {
            $element: options.element,
            $fwd: undefined,
            $back: undefined,
            $list: undefined,
            $container: undefined,
            $lastVisibleEl: undefined
        };


        init();


        return factory;


        /* /////////////////////////////////////////////////////// */




        /**
         * Add an event to the timeline
         *
         * @public
         * @param {object} event - an event object
         */
        function addEvent(event) {
            var $li = $(compileTemplate(event)).addClass('is-hidden'),
                $after = _elements.$list.find('li:nth-of-type(' + getEventInsertPoint(event.date) + ')');

            $after.after($li);

            setTimeout(function timeout() {
                $li.removeClass('is-hidden');
            }, 1);
        }


        /**
         * Remove an event from the timeline
         *
         * @public
         * @param  {object} event - the event that is to be removed
         */
        // function removeEvent(event) {
        //     // event = { date, type }
        //     var $li = _elements.$list.find('li:nth-of-type(3)');

        //     $li.addClass('is-hidden');
        //     remove the element from the DOM
        // }



        /**
         * Get the point where an event should be inserted
         *
         * @description Finds the correct index in the events array to insert
         * an event. The index is based on date and the returned date will be
         * the date immediatly preceding the index that should be used.
         *
         * @param  {string} date - a date string
         * @return {number} - the index where the new event should be inserted
         */
        function getEventInsertPoint(date) {
            var index;
            _events.data.forEach(function forEach(event, i) {
                if ( event.date < date ) {
                    index = i;
                } else {
                    return false;
                }
            });

            return index + 1;
        }



        /**
         * Destroy the timeline instance and remove all traces of it
         */
        function destroy() {
            unlink();
            _state = _elements = factory = null;
            return null;
        }





        /**
         * Move the timeline forward
         */
        function goForward() {
            if ( _state.atEnd ) { return; }

            var $gotoEl = _elements.$lastVisibleEl.next(),
                scrollPos;

            if ( !$gotoEl.length ) {
                _state.atEnd = true;
                return;
            }

            scrollPos = $gotoEl.position().left + $gotoEl.outerWidth() - _elements.$container.width();

            _elements.$container.mCustomScrollbar(
                'scrollTo',
                scrollPos,
                { scrollInertia: 300 }
            );

            _state.atBeginning = false;
            _elements.$lastVisibleEl = $gotoEl;
            _elements.$firstVisibleEl = _elements.$firstVisibleEl.next();
        }



        /**
         * Move the timeline backwards
         */
        function goBack() {
            if ( _state.atBeginning ) { return; }

            var $gotoEl = _elements.$firstVisibleEl.prev();

            if ( !$gotoEl.length ) {
                _state.atBeginning = true;
                return;
            }

            _elements.$container.mCustomScrollbar(
                'scrollTo',
                $gotoEl.position().left,
                { scrollInertia: 300 }
            );

            _state.atEnd = false;
            _elements.$lastVisibleEl = _elements.$lastVisibleEl.prev();
            _elements.$firstVisibleEl = $gotoEl;

        }



        /**
         * Initiate the timeline
         *
         * @private
         * @function init
         */
        function init() {
            var html = '';
            setupDOMElements();

            /* TODO: Refactor this for production */
            _events = options.data || getMockData();

            setTimeout(function timeout() {
                if ( isOverflowing() ) {
                    link();
                }
            }, 1);

            _events.data.forEach(function forEach(day) {
                html += compileTemplate(day);
            });

            _elements.$container.find('ul').html(html);

            setupDOMReferences();

        }



        /**
         * Setup all the DOM elements needed to layout the Timeline
         */
        function setupDOMElements() {
            _elements.$container = $('<div class="TimeLine-listContainer"></div>');
            _elements.$list = $('<ul class="TimeLine-itemlist"></ul>');
            _elements.$back = $('<div class="TimeLine-arrow left"></div>');
            _elements.$fwd  = $('<div class="TimeLine-arrow right"></div>');
            _elements.$legend = $('<div class="TimeLine-legends"><div class="TimeLine-legend"><span class="invitation"></span>Invitation</div><div class="TimeLine-legend"><span class="reminder"></span>Reminder</div><div class="TimeLine-legend"><span class="event"></span>Event</div><div class="TimeLine-legend"><span class="followup"></span>Follow up</div></div>');



            _elements.$element
                .addClass('TimeLine-container');

            _elements.$container
                .append(_elements.$list);

            _elements.$element
                .append(_elements.$container)
                .append(_elements.$legend);

            setTimeout(function waitForDOM() {
                if ( isOverflowing() ) {
                    _elements.$element
                        .append(_elements.$fwd)
                        .append(_elements.$back);

                    activateCustomScrollbars();
                }
            }, 1);
        }

        function isOverflowing() {
            return _elements.$list.width() > _elements.$container.width();
        }


        function activateCustomScrollbars() {
            _elements.$container.mCustomScrollbar({
                axis: 'x',
                theme: 'dark-2',
                scrollInertia: '1000',
                scrollButtons: { enable: false }
            });
        }


        /**
         * Save DOM references for speedy usage
         */
        function setupDOMReferences() {
            _elements.$lastVisibleEl = getLastVisibleElement();
            _elements.$firstVisibleEl = _elements.$list.find('li:nth-of-type(1)');
        }



        /**
         * Return the last visible element in the timeline
         *
         * @function getLastVisibleElement
         *
         * @return {element} - a dom reference to the last visisble element
         */
        function getLastVisibleElement() {
            var boundary = _elements.$container.scrollLeft() + _elements.$container.width(),
                $el;

            _elements.$list.find('li').each(function each(i, el) {
                var elRight = $(el).position().left + $(el).outerWidth();

                if ( false === (boundary > elRight) ) { return false; }

                $el = $(el);
            });
            return $el;
        }



        /**
         * Link DOM events to the module
         */
        function link() {
            _elements.$fwd.on('click', goForward);
            _elements.$back.on('click', goBack);
        }



        /**
         * Cancel all DOM events associated with the module
         */
        function unlink() {
            _elements.$fwd.off('click', goForward);
            _elements.$back.off('click', goBack);
        }
    }



    /**
     * Activate DOM specified timeline modules
     *
     * TODO: Add templating engine to get rid of inline templates
     */
    function activateTimelines() {

        /**
         * <div class="TimeLine-listContainer"><ul class="TimeLine-itemlist" timeline-list></ul></div></div><div class="TimeLine-legends"><div class="TimeLine-legend"><span class="invitation"></span>Invitation</div><div class="TimeLine-legend"><span class="reminder"></span>Reminder</div><div class="TimeLine-legend"><span class="event"></span>Event</div><div class="TimeLine-legend"><span class="followup"></span>Follow up</div></div>
         */

        $.each($('[data-apsis="timeline"]'), function each(i, el) {
            var $el = $(el);

            new Timeline({
                element: $el
            });

        });
    }


    /**
     * Check that all required params are passed into the constructor
     * @param  {object} options - all options passed into the constructor
     */
    function checkRequiredParams(options) {
        if ( !options ) {
            throw {
                name: 'Apsis.Timeline',
                message: 'You must define an options object'
            };
        }

        if ( !options.element ) {
            throw {
                name: 'Apsis.Timeline',
                message: 'You must define a content element'
            };
        }
    }


    function getDateString(date) {
        var months = [ 0, 'jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'okt', 'nov', 'dec'],
            monthNumber = date.substr(5, 2).replace(/0(\d)/, '$1'),
            dayNumber = date.substr(-2).replace(/0(\d)/, '$1');

        return ''  + dayNumber + ' ' + months[monthNumber];
    }








    function compileTemplate(data) {
        var template = [
            '<li>',
            '<div class="icons">'
        ],

        end = [
                '</div>',
                '<div class="date">',
                    '<span class="pipe"></span>',
                    '<time datetime="1970-10-29">{{date}}</time>'.replace(/{{date}}/, getDateString(data.date)),
                '</div>',
            '</li>'
        ];

        data.events.forEach(function each(event) {
            template.push([
                '<span class="TimeLine-badge icon-',
                event.type,
                '" data-apsis-tooltip="',
                '<strong>Reminder SMS</strong><br>',
                'Göteborg, 08:30<br>',
                'Stockholm, 08:30',
                '"><img src="../../modules/timeline/images/icon-',
                event.type,
                '.svg" class="TimeLine-icon"><span class="amount">',
                event.number,
                '</span></span>'
            ].join(''));
        });

        template = template.concat(end);

        return template.join('');
    }



    function disablePastEvents(date) {
        var today = new Date(),
            todaysDate = today.getDate(),
            dayNumber = date.substr(-2).replace(/0(\d)/, '$1');

        if (dayNumber < todaysDate) {
            $('.TimeLine-badge').addClass('is-disabled');
        } else {
            return false;
        }
    }



    function getMockData() {
        return {
            data: [
                {
                    date: 'not ready',
                    events: [
                        { id: 1, type: 'invitation', number: 3 }
                    ]
                },
                {
                    date: '2014-12-02',
                    events: [
                        { id: 3, type: 'reminder', number: 6 },
                        { id: 2, type: 'event', number: 1 }
                    ]
                },
                {
                    date: '2014-12-03',
                    events: [
                        { id: 5, type: 'reminder', number: 6 },
                        { id: 4, type: 'event', number: 4 }
                    ]
                },
                {
                    date: '2014-12-05',
                    events: [
                        { id: 6, type: 'followup', number: 5 }
                    ]
                }
            ]
        };
    }

}));


