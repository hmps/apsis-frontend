;(function iife() {
    'use strict';
    /* globals angular, $ */

    /**
     * Timeline directive
     */


    angular.module('Apsis.modules.timeline')
        .directive('apsisTimeline', timelineDirective);

    function timelineDirective(AppConfigService) {
        return {
            restrict: 'EA',
            scope: {
                data: '='
            },
            templateUrl: AppConfigService.modules.path + 'timeline/timeline.template.html',
            controllerAs: 'timeline',
            controller: timelineController,
            link: timelineLink
        };
    }


    /**
     * [timelineLink description]
     * @param  {[type]} scope [description]
     * @param  {[type]} el    [description]
     * @return {[type]}       [description]
     */
    function timelineLink(scope, $el) {
        var
        _state = {
            atEnd: false,
            atBeginnging: true
        },
        _elements = {
            $element: $el,
            $fwd: undefined,
            $back: undefined,
            $list: undefined,
            $container: undefined,
            $lastVisibleEl: undefined
        };


        /**
         * INIT: To make sure that the DOM is ready we push this to the bottom
         * of the callstack with a timeout.
         */
        setTimeout(init, 1);



        function init() {
            $el.css({
                position: 'relative'
            });

            _elements.$container = $el.find('.TimeLine-listContainer');
            _elements.$list = $el.find('.TimeLine-itemlist');
            _elements.$back = $el.find('.TimeLine-arrow.left');
            _elements.$fwd  = $el.find('.TimeLine-arrow.right');

            _elements.$fwd.on('click', goForward);
            _elements.$back.on('click', goBack);

            setupDOMReferences();

            if ( isOverflowing() ) {
                _elements.$fwd.addClass('is-visible');
                _elements.$back.addClass('is-visible');

                activateCustomScrollbars();
            }
        }



        /**
         * [activateCustomScrollbars description]
         * @return {[type]} [description]
         */
        function activateCustomScrollbars() {
            _elements.$container.mCustomScrollbar({
                axis: 'x',
                theme: 'dark-2',
                scrollInertia: '1000',
                scrollButtons: { enable: false },
                callbacks: {
                    onScroll: function onScroll() {
                        finishedScrolling(this);
                    }
                }
            });
        }



        /**
         * [finishedScrolling description]
         * @param  {[type]} el [description]
         * @return {[type]}    [description]
         */
        function finishedScrolling(el) {
            _elements.$lastVisibleEl = getLastVisibleElement(el.mcs.left);
            _elements.$firstVisibleEl = getFirstVisibleElement(el.mcs.left);
        }



        /**
         * Return the last visible element in the timeline
         *
         * @function getLastVisibleElement
         *
         * @return {element} - a dom reference to the last visisble element
         */
        function getLastVisibleElement(scroll) {
            // Scroll is given in a negative number, hence the minus
            var boundary = -(scroll || 0) + _elements.$container.width(),
                $lastEl;



            _elements.$list.find('li').each(function each(i, el) {
                var elRight = $(el).position().left + $(el).outerWidth();

                if ( false === (boundary >= elRight) ) {
                    return false;
                }

                $lastEl = $(el);
            });

            return $lastEl;
        }



        /**
         * Return the first visible element in the timeline
         *
         * @function getFirstVisibleElement
         *
         * @return {element} - a dom reference to the last visisble element
         */
        function getFirstVisibleElement(scroll) {
            var boundary = -(scroll || 0),
                $firstEl;

            _elements.$list.find('li').each(function each(i, el) {
                var elLeft = $(el).position().left;

                if ( false === (boundary >= elLeft) ) { return false; }

                $firstEl = $(el);
            });

            return $firstEl;
        }


        /**
         * Save DOM references for speedy usage
         */
        function setupDOMReferences() {
            _elements.$lastVisibleEl = getLastVisibleElement();
            _elements.$firstVisibleEl = _elements.$list.find('li:nth-of-type(1)');
        }


        /**
         * Move the timeline backwards
         */
        function goBack() {
            if ( _state.atBeginning ) { return; }

            var $gotoEl = _elements.$firstVisibleEl.prev();

            if ( !$gotoEl.length ) {
                _state.atBeginning = true;
                return;
            }

            _elements.$container.mCustomScrollbar(
                'scrollTo',
                $gotoEl.position().left,
                { scrollInertia: 300 }
            );

            _state.atEnd = false;
        }


        /**
         * [isOverflowing description]
         * @return {Boolean} [description]
         */
        function isOverflowing() {
            return _elements.$list.width() > _elements.$container.width();
        }



        /**
         * Move the timeline forward
         */
        function goForward() {
            if ( _state.atEnd ) { return; }

            var $gotoEl = _elements.$lastVisibleEl.next(),
                scrollPos;

            if ( !$gotoEl.length ) {
                _state.atEnd = true;
                return;
            }

            scrollPos = $gotoEl.position().left + $gotoEl.outerWidth() - _elements.$container.width();

            _elements.$container.mCustomScrollbar(
                'scrollTo',
                scrollPos,
                { scrollInertia: 300 }
            );

            _state.atBeginning = false;
        }
    }



    /**
     * [timelineController description]
     * @param  {[type]} $scope [description]
     * @return {[type]}        [description]
     */
    function timelineController($scope) {
        var vm = this;

        vm.data = $scope.data;

        return vm;
    }
}());
