/* global jQuery */
;(function select($) {
    'use strict';

    setTimeout(function waitForDOM() {
        $.each($('select'), replaceSelects);
    }, 50);


    function PrettySelect(el) {
        var select = this,
            _items = [],
            _activeItem = 0,
            _i;

        select.$el = $(el),
        select.$options = select.$el.children('option'),
        select.$styledSelect,
        select.$list,
        select.$listItems,

        select.$el
            .addClass('select-hidden')
            .wrap('<div class="select"></div>')
            .after('<div class="select-styled"></div>');


        select.$styledSelect = select.$el
            .next('.select-styled')
            .text(select.$options.eq(0).text());


        select.$list = $('<ul />', {
            class: 'select-options'
        })
        .insertAfter(select.$styledSelect);


        for ( _i = 0; _i < select.$options.length; _i++ ) {
            _items.push(select.$options.eq(_i).text());

            $('<li />', {
                text: select.$options.eq(_i).text(),
                rel: select.$options.eq(_i).val()
            })
            .appendTo(select.$list);
        }

        select.$listItems = select.$list.children('li');
        select.$listItems.eq(0).addClass('has-focus');

        link();



        /****************************************************/
        /* HELPER METHODS
        /****************************************************/



        function closeDropdown() {
            select.$styledSelect.removeClass('active');
            select.$list.hide();
        }



        function closeAllDropdowns() {
            var $activeSelect = $('.select-styled.active');

            $activeSelect.removeClass('active');
            $activeSelect.next('.select-options').hide();
        }


        function toggleDropdown() {
            select.$styledSelect.toggleClass('active');
            select.$list.toggle();
        }



        function setSelectedValue(value) {
            return select.$el.val(value);
        }



        function setSelectedText(text) {
            return select.$styledSelect.text(text);
        }



        function selectNextItem() {
            /* (_items.length - 1) because lenght starts on 1 and _activeItem
               starts on 0 */
            if ( ( _items.length - 1 ) > _activeItem ) {
                selectItem(_activeItem + 1);
            }
        }



        function selectPrevItem() {
            if ( 0 < _activeItem ) {
                selectItem(_activeItem - 1);
            }
        }



        function selectItem(index) {
            _activeItem = index;

            setSelectedText(_items[_activeItem]);
            setSelectedValue(_activeItem);

            select.$list.find('.has-focus').removeClass('has-focus');
            select.$list.children().eq(index).addClass('has-focus');
        }




        function link() {


            select.$el.on('keydown', function change(e) {
                /**
                 * 9 = tab
                 * 13 = enter
                 * 32 = spacebar
                 * 38 = up key
                 * 40 = down
                 */
                var capture = [9, 13, 32, 38, 40];

                /* If the key pressed is anything other than the above, we'll
                 * simply return and let the user get back to business */
                if ( capture.indexOf(e.which) === -1 ) { return; }

                /* If the tab key is pressed we close the dropdown and then move
                 * on to the next tab index. If the dropdown is not open the user
                 * will not notice any difference from normal browser behaviour */
                if ( 9 === e.which ) {
                    closeDropdown();
                    return;
                }

                e.preventDefault();
                e.stopPropagation();

                switch (e.which) {
                    case 13:
                        closeDropdown();
                        break;
                    case 32:
                        toggleDropdown();
                        break;
                    case 38:
                        selectPrevItem();
                        break;
                    case 40:
                        selectNextItem();
                        break;
                }
            });



            /**
             * [change description]
             * @return {[type]} [description]
             */
            select.$el.change(function change() {
                setSelectedText(this.selectedIndex);
            });



            /**
             * [change description]
             * @return {[type]} [description]
             */
            select.$el.focus(function change() {
                select.$styledSelect.addClass('has-focus');
            });



            /**
             * [change description]
             * @return {[type]} [description]
             */
            select.$el.blur(function change() {
                select.$styledSelect.removeClass('active');
                select.$styledSelect.removeClass('has-focus');
            });



            select.$styledSelect.click(function click(e) {
                e.stopPropagation();

                if ( !select.$styledSelect.hasClass('active') ) {
                    closeAllDropdowns();
                }

                toggleDropdown();

                select.$el.focus();
            });



            select.$listItems.click(function click(e) {
                var $this = $(this);

                e.stopPropagation();

                setSelectedText($this.text());
                setSelectedValue($this.attr('rel'));

                select.$list.hide();
            });



            $(document).click(function click() {
                select.$styledSelect.removeClass('active');
                select.$list.hide();
            });
        }
    }


    function replaceSelects(index, el) {
        if ( undefined === $(el).data('apsisNoReplace') ) {
            return new PrettySelect(el);
        } else {
            $(el).addClass('is-visible');
        }
    }

}(jQuery));
