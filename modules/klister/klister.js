/* globals define */
;(function umd(root, name, factory) {
    'use strict';
    if ('function' === typeof define && define.amd) {
        define(name, [], factory);
    } else {
        // Register as a function on the global namespace
        root[name] = factory();
    }
}(this, 'Klister', function UMDFactory() {
    'use strict';

    var Klister = function Klister(options) {
        /**
         * If Klister was not called as a constructor we help the user out by
         * having klister contructing itself.
         *
         * TODO: Should this throw a warning?
         */
        if ( this === window ) { return new Klister(options); }

        var _options = {};

        // Kick of the configuration of Klister
        init(options);

        // Return the public API
        return {
            activate: activate,
            deactivate: deactivate,
            destroy: destroy
        };


        /* ------------------------------------ */
        /* Internal methods                     */
        /* ------------------------------------ */


        /**
         * [activate description]
         * @return {[type]} [description]
         */
        function activate() {

        }


        /**
         * [deactivate description]
         * @return {[type]} [description]
         */
        function deactivate() {

        }


        /**
         * [destroy description]
         * @return {[type]} [description]
         */
        function destroy() {

        }


        /**
         * [init description]
         * @return {[type]} [description]
         */
        function init(options) {
            checkForRequiredParameters();

            _options.target = options.target;
        }


        /**
         * [checkForRequiredParameters description]
         * @return {[type]} [description]
         */
        function checkForRequiredParameters() {
            if ( !options ) {
                throw new KlisterException({
                    id: 1,
                    message: 'You need to pass an options object with the required parameters when constructing Klister'
                });
            }

            if ( !options.target ) {
                throw new KlisterException({
                    id: 2,
                    message: 'To create a Klister element you need to specify a target element'
                });
            }
        }


        /**
         * [KlisterException description]
         * @param {[type]} params [description]
         */
        function KlisterException(params) {
            this.name = 'KlisterError';
            this.id = params.id;
            this.message = params.message;
        }
    };

    return Klister;
}));
