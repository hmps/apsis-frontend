/* globals define */
;(function umd(root, name, factory) {
    'use strict';
    if ('function' === typeof define && define.amd) {
        define(name, [], factory);
    } else {
        // Register as a function on the global namespace
        root[name] = factory();
    }
}(this, 'FilterSelect', function UMDFactory(cartero) {
    'use strict';

    return function FilterSelect(list) {
        cartero.on('message', function carteroMessage() {
            console.log('message received');
        });
        var el = document.getElementById('js-filterSelect'),
            filterBox = el.getElementsByTagName('input')[0],
            vDOM = document.createDocumentFragment(),
            oDOM;

        function init() {

            var ul = document.createElement('ul');
            ul.classList.add('FS-select');
            vDOM.appendChild(ul);

            fillVDOMList(list, {key: 'company'});
            oDOM = Object.create(vDOM);
            renderVDOMToDOM(vDOM);

            setupListeners();
        }

        init();



        function resetVDOM() {
            var ul = vDOM.querySelector('ul');

            while (ul.firstChild) {
                ul.removeChild(ul.firstChild);
            }
        }


        function removeVDOMFromDOM() {
            // Counting on that the UL will be the last child of the element.
            el.removeChild(el.lastChild);
        }


        function renderVDOMToDOM(vDOM) {
            return el.appendChild(vDOM.cloneNode(true));
        }


        function fillVDOMList(data, options) {
            var ul = vDOM.querySelector('ul');

            options     = options     || {};
            options.key = options.key || 'company';

            data.forEach(function forEach(item) {
                var li = document.createElement('li');
                li.innerHTML = item[options.key];
                li.setAttribute('company', item[options.key]);
                ul.appendChild(li);
            });

            return ul;
        }


        function filterList(needle, haystack, options) {
            var filteredList;

            if ( !haystack ) { return false; }
            if ( !needle )   { return haystack; }

            options     = options     || {};
            options.key = options.key || 'title';

            filteredList = haystack.filter(function filter(item) {
                return new RegExp(needle, 'i').test(item[options.key]);
            });

            return filteredList;
        }


        function updateDOM(list) {
            resetVDOM();
            fillVDOMList(list);
            removeVDOMFromDOM();
            renderVDOMToDOM(vDOM);
        }

        /*function listIsFiltered(isFiltered) {
            if ( isFiltered ) {
                // set css rules for filtered list
                // should mean that the first item in the list is highlighted
            } else {
                // reset css rules for list
            }
        }*/




        function setupListeners() {
            filterBox.addEventListener('keydown', function keydown(e) {
                if ( 9 === e.which || 13 === e.which ) {
                    e.preventDefault();
                    e.stopPropagation();
                }
            });


            filterBox.addEventListener('keyup', function keyup(e) {
                var newList;
                e.preventDefault();
                e.stopPropagation();

                if ( '' === this.value ) {
                    // listIsFiltered(false);
                    newList = list;
                } else {
                    // listIsFiltered();
                    // filter vdom and populate new list
                    newList = filterList(this.value, list, { key: 'company' });
                }

                updateDOM(newList);
            });

            filterBox.addEventListener('focus', function focus() {
                el.classList.add('is-active');
                cartero.trigger('message');
            });

            filterBox.addEventListener('blur', function blur() {
                el.classList.remove('is-active');
            });
        }
    };
}));
