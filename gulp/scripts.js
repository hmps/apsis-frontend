;(function gulpScripts() {
    'use strict';

    var gulp = require('gulp'),

        $ = require('gulp-load-plugins')({
            pattern: ['gulp-*', 'main-bower-files']
        });


    gulp.task('scripts', ['jshint', 'jscs'], function scripts() {
        return gulp.src([
                'modules/**/*.js'
            ])
            .pipe($.size());
    });


    gulp.task('complexity', function complexity(){
        return gulp.src('modules/**/*.js')
            .pipe($.complexity());
    });

    gulp.task('lint', ['jshint', 'jscs'], function lint() {
        return gulp.src('modules/**/*.js')
            .pipe($.size());
    });


    gulp.task('jshint', function jshint() {
        return gulp.src([
                'modules/**/*.js'
            ])
            .pipe($.jshint())
            .pipe($.jshint.reporter('jshint-stylish'))
            .pipe($.jshint.reporter('fail'))
            .on('error', $.notify.onError(function onError(error) {
                return error.message;
            }));
    });

    gulp.task('jscs', function jscs() {
        return gulp.src([
                'modules/**/*.js'
            ])
            .pipe($.jscs())
            .on('error', $.notify.onError(function onError(error) {
                return error.message;
            }));
    });

}());
