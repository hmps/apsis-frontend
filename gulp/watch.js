/**
 * # WATCH GULP TASK
 * Watches files and/or folders for changes and runs a set of tasks on it.
 */

;(function gulpWatch() {
    'use strict';

    var gulp = require('gulp');

    gulp.task('watch', ['styles'], function watch() {
        gulp.watch('modules/**/*.scss', ['styles']);
        gulp.watch('modules/**/*.js');
    });

    gulp.task('watch-tdd', ['styles'], function watch() {
        gulp.start('tdd');
        gulp.watch('modules/**/*.scss', ['styles']);
        gulp.watch('modules/**/*.js', ['jshint', 'jscs']);
    });
}());
