;(function gulpStyles() {
    'use strict';

    var gulp = require('gulp'),
        $ = require('gulp-load-plugins')({
            pattern: ['gulp-*', 'main-bower-files']
        });


    gulp.task('styles', function styles() {
        return gulp.src('modules/**/*.scss')
            .pipe($.rubySass({
                require: 'compass/import-once/activate',
                style: 'expanded'
            }))
            .pipe($.autoprefixer('last 2 version'))
            .pipe(gulp.dest('examples/'))
            // .pipe(browserSync.reload({stream:true}))
            .pipe($.size());
    });

}());
