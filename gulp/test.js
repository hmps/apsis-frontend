;(function gulpTest() {
    'use strict';

    var gulp = require('gulp'),
        karma = require('karma').server;

    gulp.task('test', function test(done) {
        karma.start({
            configFile: __dirname + '/../tests/karma-unit.conf.js',
            singleRun: false
        }, done);
    });

    gulp.task('tdd', function test(done) {
        karma.start({
            configFile: __dirname + '/../tests/karma-unit.conf.js'
        }, done);
    });

}());
