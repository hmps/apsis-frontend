;(function gulpServe() {
    'use strict';

    var gulp = require('gulp'),
        browserSync = require('browser-sync');

    gulp.task('serve', ['watch'], function serve() {
        browserSync({
            server: {
                baseDir: './',
                routes: {
                    '/': 'examples/'
                }
            },
            files: [
                'modules/**/*.js',
                'examples/**/*.html',
                'examples/**/*.css',
                'examples/**/*.js'
            ],
            browser: ['google chrome canary']
        });
    });

    gulp.task('serve-tdd', ['watch-tdd'], function serve() {
        browserSync({
            server: {
                baseDir: './',
                routes: {
                    '/': 'examples/'
                }
            },
            files: [
                'modules/**/*.js',
                'examples/**/*.html',
                'examples/**/*.css',
                'examples/**/*.js'
            ],
            browser: ['google chrome canary']
        });
    });

}());
